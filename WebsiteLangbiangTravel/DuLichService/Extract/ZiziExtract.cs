﻿using DuLichService.DAO;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Extract
{
    public class ZiziExtract : Extract
    {
        public ZiziExtract(dulichvnEntities db) :base(db)
        {
            url = "http://travel.zizi.vn/thong-tin-du-lich/1/dia-danh-du-lich";
        }
        public location GetFullInfo(string url,string provinceName,ref List<resource> res)
        {
            url =@"http://travel.zizi.vn" + url;
            ProvinceDAO provinceDAO=new ProvinceDAO(db);
            
            HtmlWeb detail = new HtmlWeb();
            location location = new location();
            
            location.province=provinceDAO.GetByName(provinceName);
            
            if (location.province == null) return null;
            location.id_province = location.province.id;
            CategoryDAO categoryDAO = new CategoryDAO(db);
            location.category = categoryDAO.Get("DLH");
            location.id_category = location.category.id;
            location.geo_x = 0;
            location.geo_y = 0;
            location.rate = 0;
            location.rate_n = 0;
            HtmlDocument doc = detail.Load(url);
            var x = doc.DocumentNode.SelectSingleNode(@"//div[@class='write_post']");
            var nameNote = x.SelectSingleNode(@"./h3");

            location.name = nameNote.InnerText.Trim();
            if (location.name.Length > 1000) location.name=location.name.Substring(0, 1000);
            if (location.name.Length > 20)
                location.id = location.name.Substring(0, 20);
            else location.id = location.name;
            string info="";
            var text=x.SelectNodes(@".//text()");
            foreach (var i in text)
                info += i.InnerText.Trim();
            if (info.Length>=4000)
            info=info.Substring(0, 3999);
            location.info = info;
            
            //add resource
            try
            {
                foreach (var p in x.SelectNodes(@".//img"))
                {
                    resource r = new resource();
                    r.location = location;
                    r.id_location = location.id;
                    r.name = p.Attributes["alt"].Value.Trim();
                    if (r.name.Length >= 50) r.name = r.name.Substring(0, 49);
                    r.link = p.Attributes["src"].Value.Trim();

                    res.Add(r);
                }
            }
            catch (Exception ex) { }
            //return new location(location.id, location.name, location.info, (double)location.rate, (int)location.rate_n, (double)location.geo_x, (double)location.geo_y, location.id_province, location.id_category, location.category, location.province);
            return location;
        }
        public override List<location> Convert()
        {
            ProvinceDAO provinceDAO=new ProvinceDAO(db);
            HtmlWeb list = new HtmlWeb();
            HtmlDocument doc = list.Load(url);

            List<location> result = new List<location>();

            var page = doc.DocumentNode.SelectSingleNode(@"//li[@class='page_last']/a");
            var link1 = page.Attributes["href"].Value.Trim();
            int index=link1.IndexOf("page=");
            int npage = int.Parse(link1.Substring(index + 5));
            int curPage=1;
            do
            {
                var xxx = doc.DocumentNode.SelectNodes(@"//div[@class='info_place_tour']");
                foreach (var x in xxx)
                {
                    var linkNote = x.SelectSingleNode(@"./h6/a");
                    var provinceNote = x.SelectSingleNode(@".//p[@class='province']/text()");
                    string link = linkNote.Attributes["href"].Value.Trim();
                    string provinceName = provinceNote.InnerText.Trim();
                    List<resource> res = new List<resource>();
                    location location = GetFullInfo(link, provinceName, ref res);
                    LocationDAO dao = new LocationDAO(db);
                    ResourceDAO resDAO = new ResourceDAO(db);

                    if (location != null)
                    {

                        if (dao.AddMulti(location))
                        {
                            foreach (var r in res)
                            {
                                resDAO.AddMulti(r);
                            }
                            //result.Add(location);
                        }
                    }
                }
                curPage++;
                doc = list.Load(url + "?page="+curPage);
            } while (curPage <= npage);
            return result;
        }
       
    }
    
}