﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Extract
{
    public class ExtractHelper
    {
        private List<Extract> extracts;
        public ExtractHelper()
        {
            extracts = new List<Extract>();
        }
        public void Add(Extract ex)
        {
            extracts.Add(ex);
        }
        public List<location> listExtract()
        {
            List<location> result = new List<location>();
            foreach (Extract x in extracts)
            result.AddRange(x.Convert());
            
            return result;
        }
    }
}