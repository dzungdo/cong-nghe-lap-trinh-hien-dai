﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuLichService.DAO
{
    public interface IDAO<T>
    {
        List<T> GetList();
        T Get(string id);
        bool Add(T entry);
        bool Update(T entry);
        bool Delete(T entry);
    }
}
