﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.DAO
{
    public class CategoryDAO :DAO , IDAO<category>
    {
        public CategoryDAO(dulichvnEntities db) : base(db) { }

        public List<category> GetList()
        {
            
            return db.categories.ToList();
        }

        public category Get(string id)
        {
        
            //throw new NotImplementedException();
            try
            {
                return db.categories.Single(c => id.Contains(c.id));
            }
            catch (Exception ex) { return null; }
        }

        public bool Add(category entry)
        {
            throw new NotImplementedException();
        }

        public bool Update(category entry)
        {
            throw new NotImplementedException();
        }

        public bool Delete(category entry)
        {
            throw new NotImplementedException();
        }
        
    }
}