﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DuLichService.Models;

namespace DuLichService.DAO
{
    public class ResourceDAO : DAO, IDAO<resource>
    {
        public ResourceDAO(dulichvnEntities db) : base(db) { }
        public List<resource> GetList()
        {
            throw new NotImplementedException();
        }

        public resource Get(int id)
        {

            //throw new NotImplementedException();
            try
            {
                return db.resources.Single(r => r.id == id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Add(resource entry)
        {
            //throw new NotImplementedException();

            try
            {
                
                db.resources.Add(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool AddMulti(resource entry)
        {
            db.resources.Add(entry);
            return true;
        }
        public bool Update(resource entry)
        {
            //throw new NotImplementedException();

            try
            {
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Delete(resource entry)
        {

            //throw new NotImplementedException();
            try
            {
                db.resources.Remove(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        internal List<ResourceModel> GetList(string searchKey, string locationId, int offset, int n)
        {
    
            //throw new NotImplementedException();
            try
            {
                if (searchKey == null) searchKey = "";
                if (locationId == null) locationId = "";
                if (offset < 0) offset = 0;
                if (n < 0) n = 0;
                var result = from r in db.resources where r.name.Contains(searchKey) && r.location.id.Contains(locationId) select r;
                
                result = result.OrderByDescending(r => r.id_location).Skip(offset).Take(n);
                List<resource> list = result.ToList();
                return ResourceModel.copy(list);
            }
            catch (Exception ex)
            {

                return null;

            }
        }


        public resource Get(string id)
        {
            throw new NotImplementedException();
        }
    }
}