﻿using DuLichService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DuLichService.DAO
{
    public class AccountDAO:DAO,IDAO<account>
    {
        public AccountDAO(dulichvnEntities db) : base(db) { }

        public List<account> GetList()
        {

            return db.accounts.ToList();
        }

        public account Get(string id)
        {
    
            try
            {
                return db.accounts.Single(a => a.username.Equals(id));
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Add(account entry)
        {
               
            try
            {
                db.accounts.Add(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool Update(account entry)
        {
  

            try
            {
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) 
            {
                return false; 
            }
        }

        public List<IndexLocationModel> Favourite(string id, string searchKey, int offset, int n)
        {
  
            //DulichContext db = new DulichContext();
            if (id == null) return null;
            try
            {
                if (searchKey == null) searchKey = "";
                if (offset < 0) offset = 0;
                if (n < 0) n = 0;
                account acc = Get(id);
                if (acc != null)
                {
                    var x = from l in acc.locations where l.name.Contains(searchKey) select new IndexLocationModel(l);
                    return x.Skip(offset).Take(n).ToList();
                }
                return null;
            }
            catch (Exception ex) { return null; }
            
        }

        public List<AccountModel> GetListAccountModel(string searchKey, string role, int offset, int n)
        {
    
            try
            {
                if (searchKey == null) searchKey = "";
                if (role == null) role = "";
                
                if (offset < 0) offset = 0;
                if (n < 0) n = 0;
                var result = from a in db.accounts where a.role_account.Contains(role) && a.username.Contains(searchKey) select a;

                result = result.OrderByDescending(l => l.username).Skip(offset).Take(n);
                List<account> list = result.ToList();
                return AccountModel.copy(list);
            }
            catch (Exception ex)
            {

                return null;

            }
        }



        public bool Delete(account entry)
        {
  
            //throw new NotImplementedException();
            try
            {
                db.accounts.Remove(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}