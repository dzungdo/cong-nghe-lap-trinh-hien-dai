﻿using DuLichService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace DuLichService.DAO
{
    public class LocationDAO :DAO,IDAO<location>
    {
        public LocationDAO(dulichvnEntities db) : base(db) { }


        public List<IndexLocationModel> GetList(string searchKey, string provinceId, string categoryId, int offset, int n)
        {

            try
            {
                if (searchKey == null) searchKey = "";
                if (provinceId == null) provinceId = "";
                if (categoryId == null) categoryId = "";
                if (offset < 0) offset = 0;
                if (n < 0) n = 0;
                var result = from l in db.locations where l.province.id.Contains(provinceId) && l.name.Contains(searchKey) && l.category.id.Contains(categoryId) select l;

                result = result.OrderByDescending(l => l.rate).Skip(offset).Take(n);
                List<location> test = result.ToList();
                return IndexLocationModel.copy(test);
            }
            catch (Exception ex)
            {

                return null;

            }
        }

        public List<LocationModel> GetListLocationModel(string searchKey, string provinceId, string categoryId, int offset, int n)
        {

            try
            {
                if (searchKey == null) searchKey = "";
                if (provinceId == null) provinceId = "";
                if (categoryId == null) categoryId = "";
                if (offset < 0) offset = 0;
                if (n < 0) n = 0;
                var result = from l in db.locations where l.province.id.Contains(provinceId) && l.name.Contains(searchKey) && l.category.id.Contains(categoryId) select l;

                result = result.OrderByDescending(l => l.rate).Skip(offset).Take(n);
                List<location> list = result.ToList();
                return LocationModel.copy(list);
            }
            catch (Exception ex)
            {

                return null;

            }
        }

    //    public List<location> GetList()
    //    {
    //        DulichContext db = new DulichContext();
    //        return db.locations.ToList();
    //    }

        public bool Add(location entry)
        {
      
            //throw new NotImplementedException();
            //DAO.db = new dulichvnEntities();
            try
            {
               
                db.locations.Add(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool AddMulti(location entry)
        {
            location l = Get(entry.id);
            if (l != null) return false;
            db.locations.Add(entry);
            return true;
        }

        public bool Update(location entry)
        {
           
          
            try
            {
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool Delete(location entry)
        {
            
            try
            {
                db.locations.Remove(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public location Get(string id)
        {

            try
            {
                return db.locations.Single(l => l.id.Equals(id));
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<location> GetList()
        {
            throw new NotImplementedException();
        }
    }
}