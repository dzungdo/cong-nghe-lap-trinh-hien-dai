﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DuLichService.DAO
{
    public class ProvinceDAO : DAO,IDAO<province>
    {
        public ProvinceDAO(dulichvnEntities db) : base(db) { }
        public bool Add(province entry)
        {
    
            try
            {
                db.provinces.Add(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public List<province> GetList()
        {
   
            return db.provinces.ToList();
        }

        public bool Delete(province entry)
        {
         
            try
            {
                db.provinces.Remove(entry);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool Update(province entry)
        {


            try
            {
                db.Entry(entry).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public province Get(string id)
        {

            try
            {
                return db.provinces.Single(p => id.Contains(p.id));
            }
            catch (Exception ex) { return null; }

        }
        public province GetByName(string name)
        {

            try
            {
                return db.provinces.Single(p => name.Contains(p.name) || p.name.Contains(name));
            }
            catch (Exception ex) { return null; }
        }
    }
}