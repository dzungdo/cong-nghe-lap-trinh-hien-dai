﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.DAO
{
    public class AuthoDAO : DAO, IDAO<autho>
    {
        public AuthoDAO(dulichvnEntities db) : base(db) { }

        public autho Get(string id)
        {
  
            return db.authoes.Single(a => a.id == id);
        }

        public List<autho> GetList()
        {
            throw new NotImplementedException();
        }



        public bool Add(autho entry)
        {
            throw new NotImplementedException();
        }

        public bool Update(autho entry)
        {
            throw new NotImplementedException();
        }

        public bool Delete(autho entry)
        {
            throw new NotImplementedException();
        }
    }
}