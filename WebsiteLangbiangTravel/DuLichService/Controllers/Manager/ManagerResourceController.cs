﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DuLichService.DAO;
using DuLichService.Models;

namespace DuLichService.Controllers.Manager
{
    public class ManagerResourceController : ApiController
    {
        private dulichvnEntities db=new dulichvnEntities();
        public List<ResourceModel> GetResource(string searchKey, string locationId, int offset, int n)
        {
            ResourceDAO resourceDAO = new ResourceDAO(db);
            return resourceDAO.GetList(searchKey, locationId, offset, n);
        }

        public bool PostResource(string name, string link, string locationId)
        {
            LocationDAO locationDAO = new LocationDAO(db);
            ResourceDAO resourceDAO = new ResourceDAO(db);

            location location = locationDAO.Get(locationId);

            if (location != null)
            {
                resource resource = new resource(name, link, locationId);
                if (resourceDAO.Add(resource))
                    return true;
            }

            return false;
        }

        public bool PutResource(int id, string name, string link, string locationId)
        {
            LocationDAO locationDAO = new LocationDAO(db);
            ResourceDAO resourceDAO = new ResourceDAO(db);

            location location = locationDAO.Get(locationId);

            if (location != null)
            {
                resource resource = new resource(id, name, link, locationId);
                if (resourceDAO.Update(resource))
                    return true;
            }

            return false;
        }

        public bool DeleteResource(int id)
        {
            ResourceDAO resourceDAO = new ResourceDAO(db);

            resource resource = resourceDAO.Get(id);
            if (resource != null)
            {
                resourceDAO.Delete(resource);
                return true;
            }
            return false;
        }

        public bool PostResource(ResourceModel resourceModel)
        {
            //DulichContext db = new DulichContext();
            
            LocationDAO locationDAO = new LocationDAO(db);
            ResourceDAO resourceDAO = new ResourceDAO(db);
            

            location location = locationDAO.Get(resourceModel.id_location);

            if (location != null)
            {
                resource resource = ResourceModel.convert(resourceModel);
                if (resourceDAO.Add(resource))
                    return true;
            }
            return false;
        }

        public bool PutAccount(ResourceModel resourceModel) // Edit location
        {
            LocationDAO locationDAO = new LocationDAO(db);
            ResourceDAO resourceDAO = new ResourceDAO(db);


            location location = locationDAO.Get(resourceModel.id_location);

            if (location != null)
            {
                resource resource = ResourceModel.convert(resourceModel);
                if (resourceDAO.Update(resource))
                    return true;
            }
            return false;
        }
    
    }
}
