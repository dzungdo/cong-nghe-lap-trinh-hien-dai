﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DuLichService.DAO;
using DuLichService.Models;

namespace DuLichService.Controllers.Manager
{
    public class ManagerLocationController : ApiController
    {
        private dulichvnEntities db = new dulichvnEntities();
        //LocationDAO locationDAO = new LocationDAO();
        //ProvinceDAO provinceDAO = new ProvinceDAO();
        //CategoryDAO categoryDAO = new CategoryDAO();

        public List<LocationModel> GetLocation(string searchKey, string provinceId, string categoryId, int offset, int n)
        {
            LocationDAO locationDAO = new LocationDAO(db);
            return locationDAO.GetListLocationModel(searchKey, provinceId, categoryId, offset, n);
        }

        public bool PostLocation(string id, string name, string info, double rate, int rate_n, double geo_x, double geo_y,
                                 string id_province, string id_category)
        {
            ProvinceDAO provinceDAO = new ProvinceDAO(db);
            CategoryDAO categoryDAO = new CategoryDAO(db);
            LocationDAO locationDAO = new LocationDAO(db);

            province province = provinceDAO.Get(id_province);
            category category = categoryDAO.Get(id_category);

            if (province != null && category != null)
            {
                location location = new location(id, name, info, rate, rate_n, geo_x, geo_y, id_province, id_category, category, province);
                if (locationDAO.Add(location))
                    return true;
            }

            return false;
        }

        public bool PutLocation(string id, string name, string info, double rate, int rate_n, double geo_x, double geo_y,
                                 string id_province, string id_category)
        {
            ProvinceDAO provinceDAO = new ProvinceDAO(db);
            CategoryDAO categoryDAO = new CategoryDAO(db);
            LocationDAO locationDAO = new LocationDAO(db);

            province province = provinceDAO.Get(id_province);
            category category = categoryDAO.Get(id_category);

            if (province != null && category != null)
            {
                location location = new location(id, name, info, rate, rate_n, geo_x, geo_y, id_province, id_category, category, province);
                if (locationDAO.Update(location))
                    return true;
            }

            return false;
        }

        public bool Delete(string id = null)
        {
            LocationDAO locationDAO = new LocationDAO(db);
            location location = locationDAO.Get(id);
            if (location != null)
                if (locationDAO.Delete(location))
                    return true;
            return false;
        }

        //public bool PostLocation(LocationModel locationModel)
        //{           

        //    province province = provinceDAO.Get(locationModel.id_province);
        //    category category = categoryDAO.Get(locationModel.id_category);            

        //    if (province != null && category != null)
        //    {
        //        location location = new location(locationModel.id, locationModel.name, locationModel.info, locationModel.rate, locationModel.rate_n,
        //                                         locationModel.geo_x, locationModel.geo_y, locationModel.id_province, locationModel.id_category,
        //                                         category, province);


        //        if (locationDAO.Add(location))
        //            return true;
        //    }

        //    return false;
        //}

        //public bool PutLocation(LocationModel locationModel) // Edit location
        //{
        //    province province = provinceDAO.Get(locationModel.id_province);
        //    category category = categoryDAO.Get(locationModel.id_category);
        //    if (province != null && category != null)
        //    {
        //        location location = new location(locationModel.id, locationModel.name, locationModel.info, locationModel.rate, locationModel.rate_n,
        //                                         locationModel.geo_x, locationModel.geo_y, locationModel.id_province, locationModel.id_category,
        //                                         category, province);
        //        if (locationDAO.Update(location))
        //            return true;
        //    }
        //    return false;
        //}

        public bool PostLocation(LocationModel locationModel)
        {
            ProvinceDAO provinceDAO = new ProvinceDAO(db);
            CategoryDAO categoryDAO = new CategoryDAO(db);
            LocationDAO locationDAO = new LocationDAO(db);

            province province = provinceDAO.Get(locationModel.id_province);
            category category = categoryDAO.Get(locationModel.id_category);

            if (province != null && category != null)
            {
                
                location location = LocationModel.convert(locationModel, category, province);

                if (locationDAO.Add(location))
                    return true;
            }

            return false;
        }

        public bool PostLocation(LocationModel locationModel, List<ResourceModel> resources)
        {
            ProvinceDAO provinceDAO = new ProvinceDAO(db);
            CategoryDAO categoryDAO = new CategoryDAO(db);
            LocationDAO locationDAO = new LocationDAO(db);
            ResourceDAO resourceDAO = new ResourceDAO(db);

            province province = provinceDAO.Get(locationModel.id_province);
            category category = categoryDAO.Get(locationModel.id_category);

            if (province != null && category != null)
            {

                location location = LocationModel.convert(locationModel, category, province);

                if (locationDAO.Add(location))
                {
                    if (resources != null)
                        foreach (ResourceModel resourceModel in resources)
                        {
                            resourceModel.name = location.name;
                            resourceModel.id_location = location.id;
                            resource resource = ResourceModel.convert(resourceModel);
                            if(!resourceDAO.Add(resource))
                                return false;
                        }
                    return true;
                }
            }

            return false;
        }

        public bool PutLocation(LocationModel locationModel) // Edit location
        {
            ProvinceDAO provinceDAO = new ProvinceDAO(db);
            CategoryDAO categoryDAO = new CategoryDAO(db);
            LocationDAO locationDAO = new LocationDAO(db);

            province province = provinceDAO.Get(locationModel.id_province);
            category category = categoryDAO.Get(locationModel.id_category);

            if (province != null && category != null)
            {                
                location location = LocationModel.convert(locationModel, category, province);

                if (locationDAO.Update(location))
                    return true;
            }
            return false;
        }
    }
}
