﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DuLichService.DAO;
using DuLichService.Models;

namespace DuLichService.Controllers.Manager
{
    public class ManagerAccountController : ApiController
    {
        private dulichvnEntities db = new dulichvnEntities();
        public List<AccountModel> GetAccount(string searchKey, string role, int offset, int n)
        {            
            AccountDAO accountDAO = new AccountDAO(db);
            return accountDAO.GetListAccountModel(searchKey, role, offset, n);
        }

        public bool PostAccount(string username, string password, string email, string role_account, string fullname)
        {            
            AuthoDAO authoDAO = new AuthoDAO(db);
            AccountDAO accountDAO = new AccountDAO(db);
                        
            autho autho = authoDAO.Get(role_account);

            if (autho != null)
            {
                //account account = new account();
                account account = new account(username, password, email, role_account, fullname, autho);                
                if (accountDAO.Add(account))
                    return true;                
            }

            return false;
        }

        

        //public bool PostAccount(AccountModel accountModel)
        //{
        //    DulichContext db = new DulichContext();
        //    AuthoDAO authoDAO = new AuthoDAO();
        //    AccountDAO accountDAO = new AccountDAO();
                        
        //    autho autho = authoDAO.Get(db, accountModel.role_account);

        //    if (autho != null)
        //    {
        //        account account = new account(accountModel.username, accountModel.password, accountModel.email, accountModel.role_account, accountModel.full_name, autho);
        //        //account account = new account();
        //        if (accountDAO.Add(db, account))
        //            return true;
        //    }
        //    return false;
        //}

        //public bool PutAccount(AccountModel accountModel) // Edit location
        //{
        //    DulichContext db = new DulichContext();
        //    AuthoDAO authoDAO = new AuthoDAO();
        //    AccountDAO accountDAO = new AccountDAO();

        //    autho autho = authoDAO.Get(db, accountModel.role_account);

        //    if (autho != null)            
        //    {
        //        account account = new account(accountModel.username, accountModel.password, accountModel.email, accountModel.role_account, accountModel.full_name, autho);
        //        //account account = new account();
        //        if (accountDAO.Update(db, account))
        //            return true;
        //    }
        //    return false;
        //}

        public bool PutAccount(string username, string password, string email, string role_account, string fullname) // Edit location
        {            
            AuthoDAO authoDAO = new AuthoDAO(db);
            AccountDAO accountDAO = new AccountDAO(db);

            autho autho = authoDAO.Get(role_account);

            if (autho != null)
            {
                account account = new account(username, password, email, role_account, fullname, autho);                
                if (accountDAO.Update(account))
                    return true;
            }
            return false;
        }

        public bool DeleteAccount(string id = null)
        {
            AccountDAO accountDAO = new AccountDAO(db);
            account acc = accountDAO.Get(id);
            if (acc != null)
                if (accountDAO.Delete(acc))
                    return true;
            return false;
        }

        public bool PostAccount(AccountModel accountModel)
        {
            //DulichContext db = new DulichContext();
            AuthoDAO authoDAO = new AuthoDAO(db);
            AccountDAO accountDAO = new AccountDAO(db);

            autho autho = authoDAO.Get(accountModel.role_account);

            if (autho != null)
            {

                account account = AccountModel.convert(accountModel, autho);
                if (accountDAO.Add(account))
                    return true;
            }
            return false;
        }

        public bool PutAccount(AccountModel accountModel) // Edit location
        {
            //DulichContext db = new DulichContext();
            AuthoDAO authoDAO = new AuthoDAO(db);
            AccountDAO accountDAO = new AccountDAO(db);

            autho autho = authoDAO.Get(accountModel.role_account);

            if (autho != null)
            {

                account account = AccountModel.convert(accountModel, autho);
                if (accountDAO.Update(account))
                    return true;
            }
            return false;
        }
    }
}
