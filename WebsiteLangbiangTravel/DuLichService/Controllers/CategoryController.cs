﻿using DuLichService.DAO;
using DuLichService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DuLichService.Controllers
{
    public class CategoryController : ApiController
    {
        private dulichvnEntities db = new dulichvnEntities();
        public List<CategoryModel> GetListProvince()
        {
            CategoryDAO categoryDAO = new CategoryDAO(db);
            return CategoryModel.copy(categoryDAO.GetList());
        }
    }
}