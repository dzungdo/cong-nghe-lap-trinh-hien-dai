﻿using DuLichService.DAO;
using DuLichService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DuLichService.Controllers
{
    public class FavouriteController : ApiController
    {
        private dulichvnEntities db = new dulichvnEntities();
        public List<IndexLocationModel> GetFavourite(string id, string searchKey, int offset, int n)
        {
            AccountDAO accountDAO = new AccountDAO(db);
            return accountDAO.Favourite(id, searchKey, offset, n);
        }

        public bool GetFavourite(string userId, string locationId)
        {
            AccountDAO accountDAO = new AccountDAO(db);
            LocationDAO locationDAO = new LocationDAO(db);
            location location = locationDAO.Get(locationId);
            account acc = accountDAO.Get(userId);
            if (acc != null && location != null)
            {
                acc.locations.Add(location);
                if (accountDAO.Update(acc))
                    return true;
                else return false;
            }
            return false;
        }
    }
}
