﻿using DuLichService.Extract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DuLichService.Controllers
{
    public class HomeController : Controller
    {
        private dulichvnEntities db = new dulichvnEntities();
        public ActionResult Index()
        {
            


            return View();
        }
        public ActionResult Extract()
        {
            ExtractHelper helper = new ExtractHelper();
            helper.Add(new ZiziExtract(db));
            List<location> list = helper.listExtract();
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            ViewBag.Title = "Extracted";
            return View();
        }
    }
}
