﻿using DuLichService.DAO;
using DuLichService.Models;
using OAuth2.Demo.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DuLichService.Controllers
{
    [NoCache]
    public class LocationController : ApiController
    {
        private dulichvnEntities db = new dulichvnEntities();
         
        public List<IndexLocationModel> GetIndexLocation(string searchKey, string provinceId, string categoryId, int offset, int n)
        {
            LocationDAO locationDAO = new LocationDAO(db);
            return locationDAO.GetList(searchKey, provinceId, categoryId, offset, n);

        }

        public LocationModel GetDetail(string id)
        {
            LocationDAO locationDAO = new LocationDAO(db);
            return new LocationModel(locationDAO.Get(id));
        }

        public bool GetRate(string locationId, int rate)
        {
            LocationDAO locationDao = new LocationDAO(db);
            location l = locationDao.Get(locationId);
            if (l != null)
            { 
                l.rate_n+=1;
                l.rate= l.rate+ (rate-l.rate)/(l.rate_n);

                if (locationDao.Update(l))
                    return true;
                
            }
            return false;

        }
    }
}
