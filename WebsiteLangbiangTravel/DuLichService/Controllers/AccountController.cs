﻿using DuLichService.DAO;
using DuLichService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DuLichService.Controllers
{
    public class AccountController : ApiController
    {
        //public bool GetCheck(string id)
        //{
        //    AccountDAO accountDAO = new AccountDAO();
        //    account acc = accountDAO.Get(id);
        //    if (acc != null) return true;
        //    return false;
        //}
        private dulichvnEntities db = new dulichvnEntities();
        public AccountModel GetCheck(string id)
        {
            AccountDAO accountDAO = new AccountDAO(db);
            account acc = accountDAO.Get(id);
            
            if (acc != null)
            {
                AccountModel accountModel = new AccountModel(acc);
                return accountModel;
            }
            return null;
        }

        public bool PostRegister(AccountModel acc)
        {
            AccountDAO accountDAO = new AccountDAO(db);
            return accountDAO.Add(acc.Convert());
        }


        public AccountModel GetAccount(string id, string pass)
        {
            AccountDAO accountDAO = new AccountDAO(db);
            account acc = accountDAO.Get(id);

            if (acc != null)
            {
                if (acc.password.Equals(pass))
                {
                    AccountModel accModel = new AccountModel(acc);
                    return accModel;
                }
            }

            return null;
        }

        //public bool PostAccount(AccountModel accountModel)
        //{
        //    AccountDAO accountDAO = new AccountDAO();
        //    AuthoDAO authoDAO = new AuthoDAO();

        //    bool exist = GetCheck(accountModel.username);
        //    if (!exist)
        //    {
        //        autho autho = authoDAO.Get("US");

        //        account account = AccountModel.convert(accountModel, autho);
        //        if (accountDAO.Add(account))
        //            return true;
        //    }
        //    return false;
        //}

    }
}
