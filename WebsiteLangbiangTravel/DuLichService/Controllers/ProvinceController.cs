﻿using DuLichService.DAO;
using DuLichService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DuLichService.Controllers
{
    public class ProvinceController : ApiController
    {
        private dulichvnEntities db = new dulichvnEntities();
      
        public List<ProvinceModel> GetListProvince()
        {
            ProvinceDAO provinceDAO = new ProvinceDAO(db);
            return ProvinceModel.copy(provinceDAO.GetList());
        }
    }
}
