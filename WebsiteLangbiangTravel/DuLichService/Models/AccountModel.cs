﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class AccountModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string role_account { get; set; }
        public string full_name { get; set; }

        public AuthoModel autho { get; set; }
        public AccountModel()
        { }


        public AccountModel(account acc)
        {
            try
            {
                username = acc.username;
                password = acc.password;
                email = acc.email;
                role_account = acc.role_account;
                full_name = acc.full_name;
                autho = new AuthoModel(acc.autho);
            }
            catch (Exception ex) { }
        }
        public account Convert()
        {
            account result = new account();
            result.username = username;
            result.password = password;
            result.full_name = full_name;
            result.email = email;
            result.role_account = role_account;
            return result;
        }

        public static List<AccountModel> copy(List<account> list)
        {
            //throw new NotImplementedException();
            if (list == null)
                return null;
            List<AccountModel> r = new List<AccountModel>();
            foreach (var x in list)
                r.Add(new AccountModel(x));
            return r;
        }

        public static account convert(AccountModel accountModel, autho autho)
        {
            account account = new account(accountModel.username, accountModel.password, accountModel.email, accountModel.role_account, accountModel.full_name, autho);
            return account;
        }
    }
}