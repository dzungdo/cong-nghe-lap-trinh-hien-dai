﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class LocationModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string info { get; set; }
        public double rate { get; set; }
        public int rate_n { get; set; }
        public double geo_x { get; set; }
        public double geo_y { get; set; }
        public int favourite { get; set; }
        public string id_province { get; set; }
        public string id_category { get; set; }
        public CategoryModel category { get; set; }
        public ProvinceModel province { get; set; }
        public List<ResourceModel> resources { get; set; }
        public LocationModel() { }
        public LocationModel(location l)
        {
            try
            {
                id = l.id;
                name = l.name;
                info = l.info;
                if (!l.rate.HasValue) l.rate = 0;
                if (!l.rate_n.HasValue) l.rate_n = 0;
                if (!l.geo_x.HasValue) l.geo_x = 0;
                if (!l.geo_y.HasValue) l.geo_y = 0;

                rate = (double)l.rate;
                rate_n = (int)l.rate_n;
                geo_x = (double)l.geo_x;
                geo_y = (double)l.geo_y;
                id_category = l.id_category;
                id_province = l.id_province;
                category = new CategoryModel(l.category);
                province = new ProvinceModel(l.province);
                if (l.accounts != null)
                    favourite = l.accounts.Count;
                if (l.resources != null)
                    resources = ResourceModel.copy(l.resources.ToList());
                else resources = null;
            }
            catch (Exception ex) { }
        }

        public static List<LocationModel> copy(List<location> list)
        {
            if (list == null) return null;
            List<LocationModel> r = new List<LocationModel>();
            foreach (var x in list)
                r.Add(new LocationModel(x));
            return r;
        }

        public static location convert(LocationModel locationModel, category category, province province)
        {
            location location = new location(locationModel.id, locationModel.name, locationModel.info, locationModel.rate, locationModel.rate_n,
                                             locationModel.geo_x, locationModel.geo_y, locationModel.id_province, locationModel.id_category,
                                             category, province);

            return location;
        }

    }
}