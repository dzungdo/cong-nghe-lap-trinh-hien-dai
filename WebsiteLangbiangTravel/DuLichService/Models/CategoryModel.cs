﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class CategoryModel
    {

        public string id { get; set; }
        public string name { get; set; }

        public CategoryModel()
        { }
        public CategoryModel(category category)
        {
            try
            {
                id = category.id;
                name = category.name;
            }catch (Exception ex){}
        }
        public static List<CategoryModel> copy(List<category> list)
        {
            if (list == null) return null;
            List<CategoryModel> r=new List<CategoryModel>();
            foreach (var x in list)
                r.Add(new CategoryModel(x));
            return r;
        }
    }
}