﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class ProvinceModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string info { get; set; }
        public string pic { get; set; }

        public ProvinceModel()
        { }

        public ProvinceModel(province p)
        {
            try
            {
                id = p.id;
                name = p.name;
                info = p.info;
                pic = p.pic;
            }
            catch { }
        }

        public static List<ProvinceModel> copy(List<province> list)
        {
            if (list == null) return null;
            List<ProvinceModel> result=new List<ProvinceModel>();

            foreach (var x in list)
                result.Add(new ProvinceModel(x));
            return result;

        }
    }
}