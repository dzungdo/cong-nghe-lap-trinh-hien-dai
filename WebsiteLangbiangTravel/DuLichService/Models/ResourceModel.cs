﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class ResourceModel
    {
        public string name { get; set; }
        public string link { get; set; }
        public string id_location { get; set; }

        public ResourceModel(resource resource)
        {
            try
            {
                name = resource.name;
                link = resource.link;
            }
            catch (Exception ex) { }
        }
        public ResourceModel() { }
        public static List<ResourceModel> copy(List<resource> list)
        {
            if (list == null) return null;
            List<ResourceModel> result = new List<ResourceModel>();

            foreach (var x in list)
                result.Add(new ResourceModel(x));
            return result;

        }

        public static resource convert(ResourceModel resourceModel)
        {
            resource resource = new resource(resourceModel.name, resourceModel.link, resourceModel.id_location);
            return resource;
        }
    }
}