﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class IndexLocationModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public double rate { get; set; }
        public string pic { get; set; }
        public string province { get; set; }

        public IndexLocationModel(location location)
        {
            try
            {
                this.id = location.id;
                this.name = location.name;
                if (!location.rate.HasValue) rate = 0;
                this.rate = (Double)location.rate;
                if (location.resources != null)
                    this.pic = location.resources.FirstOrDefault().link;
                else this.pic = null;
                if (location.province!=null)
                this.province = location.province.name;
            }
            catch (Exception) { }
        }
        public IndexLocationModel()
        { }

        public static List<IndexLocationModel> copy(List<location> list)
        {
            if (list == null) return null;
            List<IndexLocationModel> r = new List<IndexLocationModel>();
            foreach (var x in list)
                r.Add(new IndexLocationModel(x));
            return r;
        }

    }


}