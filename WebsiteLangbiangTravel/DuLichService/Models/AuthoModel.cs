﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class AuthoModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public AuthoModel() { }
        public AuthoModel(autho autho)
        {
            try
            {
                id = autho.id;
                name = autho.name;
            }
            catch (Exception ex) { }
        }

    }
}