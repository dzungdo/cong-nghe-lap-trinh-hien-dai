﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Net.Http.Headers;
using DuLichService.Models;
using LangbiangWeb.Helper;
using LangbiangWeb.Models;

namespace LangbiangWeb.Controllers
{
    public class ProvinceController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetProvinces()
        {
            try
            {
                List<Province> l = Transfer.GetProvince();
                return Json(Tools.ProvinceHtml(l));
            }
            catch (Exception ex)
            {
                return null;
            }            
        }
    }
}
