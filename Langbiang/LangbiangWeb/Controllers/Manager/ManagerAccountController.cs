﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;
using DuLichService.Models;

namespace LangbiangWeb.Controllers
{
    public class ManagerAccountController : Controller
    {

        private string url = "http://travelservice.apphb.com/";

        public ActionResult Index()
        {
            List<AccountModel> accountModels = new List<AccountModel>();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/ManagerAccount?searchKey=&role=&offset=0&n=100").Result;

            if (response.IsSuccessStatusCode)
            {
                var accounts = response.Content.ReadAsAsync<List<AccountModel>>().Result;
                if (accounts != null)
                {
                    foreach (AccountModel acc in accounts)
                        accountModels.Add(acc);
                    
                }
            }
            return View(accountModels);
               
        }

        public ActionResult Edit(string id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/Account?id="+id).Result;

            if (response.IsSuccessStatusCode)
            {
                AccountModel accountModel = response.Content.ReadAsAsync<AccountModel>().Result;
                if (accountModel != null)
                    return View(accountModel);
            }
            
            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult Edit(AccountModel accountModel)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.PutAsJsonAsync("api/ManagerAccount", accountModel).Result;

            if (response.IsSuccessStatusCode)
            {
                if(response.Content.ReadAsAsync<bool>().Result)
                    return RedirectToAction("Index");
            }
            return View(accountModel);
        }
        
        public ActionResult Delete(string id)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.DeleteAsync("api/ManagerAccount?id="+id).Result;

            if (response.IsSuccessStatusCode)
            {
                if (response.Content.ReadAsAsync<bool>().Result)
                    return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
