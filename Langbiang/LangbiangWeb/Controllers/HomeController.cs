﻿using LangbiangWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Net.Http;
using System.Net.Http.Headers;
using DuLichService.Models;
using LangbiangWeb.Helper;
using Facebook;

namespace LangbiangWeb.Controllers
{
    public class HomeController : Controller
    {
        private string AppId = "439070702888237";
        private string AppUrl = "http://localhost:64004/";
        private string AppSecret = "ef7be22c9af04dd4dcd7a06c6d8c2347";


        //private string AppId = "678910785493315";
        //private string AppUrl = "http://travel-3.apphb.com/";
        //private string AppSecret = "f985c81d82dc94f04a1b39fa5874d6f3";

        public ActionResult AccessToken()
        {
            var fb = new FacebookClient();
            string code = HttpContext.Request["code"];
            if (code == null) return View("Error");
            dynamic parameters = fb.Get("https://graph.facebook.com/oauth/access_token?client_id=" + AppId + "&redirect_uri=" + AppUrl + "Home/AccessToken&client_secret=" + AppSecret + "&code=" + code);
            try
            {
                string access_token = parameters.access_token.ToString();
                if (access_token == null) return RedirectToAction("Index"); 
                fb.AccessToken=access_token;
                dynamic x = fb.Get("/me");
                if (x != null)
                {
                    AccountModel acc = Transfer.CheckAccount((string)x.username);
                    if (acc == null)
                    {
                        acc = new AccountModel((string)x.username, (string)x.username, (string)x.email, "US", (string)x.name);

                        if (Transfer.PostRegister(acc))
                        {
                            Session["user"] = acc;
                            Session["username"] = acc.username;
                            Session["role"] = "US";
                        }
                        else return RedirectToAction("Index"); 
                    }
                    else
                    {
                        Session["user"]=acc;
                        Session["username"]=acc.username;
                        Session["role"] = acc.autho.name;
                    }
                }
            }
            catch (Exception ex)
            {
                return View("Error");
            }
            return RedirectToAction("Index");
        }
               
        public ActionResult Oauth()
        {
            string scope = "user_friends,email";
            return Redirect("https://www.facebook.com/dialog/oauth?client_id=" + AppId + "&redirect_uri=" + AppUrl + "Home/AccessToken&scope=" + scope);
        }

        public ActionResult AccessTokenService()
        {
            var access=Transfer.getAccessToken("tom", "tom");
            if (access != null)
            {
                @ViewBag.AccessToken = access;
                return View();
            }
            return View("Error");
        }

        private int _iPage = 3;
        public ActionResult Index(string searchKey="",string provinceId="",string categoryId="",int offset=0)
        {
            //Access_Token();

          ViewLocation vm=new ViewLocation();
            vm.Locations=Transfer.GetIndexLocation(searchKey,provinceId,categoryId,offset,_iPage);
            if (vm.Locations != null)
            {

                TempData["offset"] = offset;
                ViewBag.searchKey = searchKey;
                ViewBag.provinceId = provinceId;
                ViewBag.categoryId = categoryId;
                return View(vm);
            }
            else return View("Error");  //Error 
        }

        public ActionResult Search(string searchKey = "",string provinceId ="",string categoryId="",int offset=0)
        {
            return RedirectToAction("Index", new { searchKey = searchKey, provinceId = provinceId, categoryId = categoryId, offset = offset });
        }

        public ActionResult Detail(string id,string result=null)
        {
            if (id != null)
            {
                ViewBag.result = result;
                ViewBag.locationId = id;
                return View(Transfer.GetDetail(id));
            }
            return View("Error");//Error
        }

        public ActionResult AddFavourite(string locationId)
        {
            string r = null;
            if (Session["user"] != null)
            {
                string userId = (string)Session["username"];
                bool result= Transfer.GetFavourite(userId, locationId);
                if (!result) r = "Thêm không thành công";
                else r = "Thêm thành công";
            }
            else
            r = "Bạn vui lòng đăng nhập để thêm";
            return RedirectToAction("Detail", new { id = locationId,result=r });
        }

        public ActionResult AddRate(string locationId, int rate)
        {
            string r = null;
            if (Session["user"] != null)
            {
                bool result = Transfer.GetRate(locationId, rate);
                if (!result) r = "Đánh giá không thành công";
                else r = "Đánh giá thành công";
            }
            else
                r = "Bạn vui lòng đăng nhập để đánh giá";
            return RedirectToAction("Detail", new { id = locationId, result = r });
        
        }
        //favourite
        public ActionResult Favourite(string searchKey="",int offset=0)
        {
            if (Session["user"] != null)
            {
                TempData["offsetFavourite"] = offset;
                ViewBag.searchKey = searchKey;
                string id = (string)Session["username"];
                return View(new ViewLocation(Transfer.GetFavourite(id, searchKey, offset, _iPage)));
            }
            else return View("Error");
        }
        [HttpPost]
        public JsonResult GetFavourites(string searchKey="")
        {
            try
            {
                int offset = (int)TempData["offsetFavourite"];
                string id = (string)Session["username"];
                List<IndexLocationModel> l = Transfer.GetFavourite(id, searchKey, offset+ _iPage, _iPage);
                if (l.Count == _iPage)
                    offset += _iPage;
                else offset += l.Count;
                TempData["offsetFavourite"] = offset;
                return Json(Tools.FormatHtml(l));
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [HttpPost]
        public JsonResult GetLocations(string searchKey, string provinceId, string categoryId)
        {
            try
            {
                int offset=(int)TempData["offset"];
                
                List<IndexLocationModel> l=Transfer.GetIndexLocation(searchKey,provinceId,categoryId,offset+_iPage,_iPage);
                if (l.Count == _iPage)
                    offset += _iPage;
                else offset += l.Count;
                TempData["offset"] = offset;
                return Json(Tools.FormatHtml(l));
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }
     

        [HttpPost]
        public ActionResult Logon(string provider, string returnUrl)
        {
            return new ExternerLoginResult(provider,
            Url.Action("LogonCallBack", new { returnUrl }));
        }

        [HttpPost]
        public ActionResult LogonCallBack(string returnUrl)
        {
            AuthenticationResult result =
            OAuthWebSecurity.VerifyAuthentication(
            Url.Action("LogonCallBack", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
                return new EmptyResult();
            ViewBag.UserName = result.UserName;
            return View("LoginConfirm");
        }
        
        #region
        internal class ExternerLoginResult : ActionResult
        {
            public ExternerLoginResult(string _provider,
             string _returnUrl)
            {
                this.Provider = _provider;
                this.ReturnUrl = _returnUrl;
            }

            public string Provider { get; set; }
            public string ReturnUrl { get; set; }
            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(this.Provider,
                 this.ReturnUrl);
            }
        }
        #endregion  
    }
}
