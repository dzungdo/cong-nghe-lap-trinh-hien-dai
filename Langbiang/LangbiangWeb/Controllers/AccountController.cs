﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using LangbiangWeb.Filters;
using LangbiangWeb.Models;
using DotNetOpenAuth.AspNet;
using LangbiangWeb.App_Start.MyApp.UI.Infrastructure;
using System.Net.Http;
using System.Net.Http.Headers;
using DuLichService.Models;
using LangbiangWeb.Helper;

namespace LangbiangWeb.Controllers
{
     [AllowAnonymous]
    public class AccountController : Controller
    {
         private string url = "http://travelservice.apphb.com/";
        //
        // GET: /Account/Login
         public ActionResult Info(string result=null)
         {
             if (Session["user"] != null)
             {
                 ViewBag.result = result;
                 AccountModel acc = (AccountModel)Session["user"];
                 return View(acc);
             }
             return View("Error");
         }
         [HttpPost]
         public ActionResult ChangeInfo(string fullname, string email)
         {
             AccountModel acc = (AccountModel)Session["user"];
             acc.full_name = fullname;
             acc.email = email;
             if (Transfer.UpdateAccount(acc))
                 return RedirectToAction("Info", new { result = "Change Successfull" });
             return RedirectToAction("Info", new { result = "Change Unsuccessfull" });
             
 
         }
         [HttpPost]
         public ActionResult ChangePassword(string password, string repassword)
         {
             if (password.Equals(repassword))
             {
                 AccountModel acc = (AccountModel)Session["user"];
                 acc.password = password;  
                 
                 if (Transfer.UpdateAccount(acc))
                     return RedirectToAction("Info", new { result = "Change Successfull" });
                 return RedirectToAction("Info", new { result = "Change Unsuccessfull" });
             }
             return RedirectToAction("Info", new { result = "Change Unsuccessfull" });
         }

         [HttpPost]
         public JsonResult Login(LoginModel model)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/account?id="+model.UserName+"&pass="+model.Password).Result;

            if (response.IsSuccessStatusCode)
            {
                var acc = response.Content.ReadAsAsync<AccountModel>().Result;
                if (acc != null)
                {
                    Session["user"] = acc;
                    Session["username"] = acc.username;
                    Session["role"] = acc.autho.name;
                    return Json(new { status = "true" });
                    
                }
            }
            return Json(new { status = "false" });
        }

        //
        // POST: /Account/Login


        //public JsonResult Login(LoginModel model, string returnUrl)
        //{
        //    if (WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe)){
        //        return Json(new { status = "true" });
        //    }
        //    return Json(new { status = "false" });
        //}

        //
        // POST: /Account/LogOff

        [HttpPost]
        public ActionResult LogOff()
        {
            Session["user"] = null;
            Session["username"] = null;
            Session["role"] = null;                    
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

   
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        //[HttpPost]
        //[AllowAnonymous]
        //public JsonResult Register(RegisterModel model)
        //{
        //    // Attempt to register the user
        //    try
        //    {
        //        WebSecurity.CreateUserAndAccount(model.UserName, model.Password, propertyValues: new
        //        {
        //            Email = model.Email,
        //            FullName = model.FullName
        //        });
        //        WebSecurity.Login(model.UserName, model.Password);
        //        //return RedirectToAction("Default", "Shared");
        //        return Json(new { status = "true" });
        //    }
        //    catch
        //    {
        //        return Json(new { status = "false" });
        //    }
        //}


        [HttpPost]        
        public JsonResult Register(RegisterModel model)
        {
            // Attempt to register the user
            try
            {
                //return RedirectToAction("Default", "Shared");
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                AccountModel accountModel = new AccountModel(model.UserName, model.Password, model.Email, "US", model.FullName);

                HttpResponseMessage response = client.PostAsJsonAsync("api/account", accountModel).Result;
                if (response.IsSuccessStatusCode)
                    return Json(new { status = "true" });
                else
                    return Json(new { status = "false" });

            }
            catch
            {
                return Json(new { status = "false" });
            }
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Only disassociate the account if the currently logged in user is the owner
            if (ownerAccount == User.Identity.Name)
            {
                // Use a transaction to prevent the user from deleting their last login credential
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage

        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");

            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", String.Format("Unable to create local account. An account with the name \"{0}\" may already exist.", User.Identity.Name));
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return RedirectToAction("Oauth", "Home", null);
            //return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToLocal(returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
