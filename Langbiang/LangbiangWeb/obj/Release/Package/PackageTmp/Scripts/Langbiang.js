﻿
//Nhấp vào nút login để hiển thị lightbox
$(".link-opensignup").on('click', function () {
    $("#lightbox").fadeIn('fast');
    $("#lgfb_regis").show();
    $("#loginbt").hide();
    $(".lblsignup").show();
    $(".login-container").show();
    $("#register").hide();
    $(".fblogincontainer").show();
});

//Đóng lightbox
$(".close").on('click', function () {
    $("#lightbox").fadeOut('fast');
});

//Hiển thị form đăng nhập
$(".login-container").on('click', function () {
    $("#lgfb_regis").hide();
    $("#loginbt").show();
    $("#lgfb_regis").hide();
    $(".txt-Username").val('Username');
    $(".txt-Password").val('Password');
    $(".lblsignup").hide();
    $(".login-container").hide();
});

$("#user").on('click', function () {
    $(".txt-Username").val('');
    $(".txt-Password").val('Password');
});

$("#pass").on('click', function () {
    $(".txt-Username").val('Username');
    $(".txt-Password").val('');

});

$(".link-signup").on('click', function () {
    $("#register").show();
    $("#loginbt").hide();
    $("#lgfb_regis").hide();
    $(".fblogincontainer").hide();
    $(".login-container").hide();
});

//Đăng nhập
$("#btn-login").on('click', function () {
    $.ajax({
        url: "/Account/Login",
        type: "POST",
        data: $("#loginform").serializeArray()
    }).done(function (data) {
        if (data.status == "true") {
            location.reload();
        }
    });
});

//$("#btn-login-facebook").on('click', function () {
//    $.ajax({
//        url: "/Account/LoginFacebook",
//        type: "GET",
//        data: $("#loginform").serializeArray()
//    }).done(function (data) {
//        if (data.status == "true") {
//            location.reload();
//        }
//    });
//});

//Đăng xuất
$(".link-logout").on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: "/Account/LogOff",
        type: "POST",
        data: $("#logoutForm").serializeArray()
    }).done(function () {
        location.reload();
    });
});

//Đăng ký
$("#btn-register").on('click', function () {
    $.ajax({
        url: "/Account/Register",
        type: "POST",
        data: $("#registerForm").serializeArray()
    }).done(function (data) {
        if (data.status == "true") {
            $("#lightbox").fadeOut('fast');
            location.reload();
        }
    });
});

//Form đăng ký
$(".link-login").on('click', function () {
    $("#register").hide();
    $("#loginbt").show();
    $("#lgfb_regis").hide();
    $(".fblogincontainer").show();
});

//Slide
$('.bxslider').bxSlider({
    auto: true,
    mode: 'fade'
});

$(document).ready(function () {

    $('#dropdowninfo').hide();
    $('#dropdowndish').hide();
});

$("#link-drop-menu").on('mouseenter', function () {
    $('#dropdowninfo').fadeIn("slow");
});

$("#dropdowninfo").on('mouseleave', function () {
    $(this).hide();
});

$("#dropdowndish").on('mouseleave', function () {
    $(this).hide();
});

//Chọn tab trong danh sách món ăn
$(".select").click(function (e) {
    $(".select").removeClass("selected");
    $(this).addClass('selected');
});

//Chọn loại món ăn trong danh sách món ăn
$("#selectdish").click(function (e) {
    $("#dropdowndish").removeClass('close');
    $("#dropdowndish").addClass('open');
    $("#dropdowndish").css('display', 'block');
});

//Chọn hình món ăn
$("#uploadhinh").change(function (e) {
    var filename = $(this).val();
    var temp = filename.split('\\');

    var reader = new FileReader();
    reader.onload = function (e) {
        $('#blah').attr('src', e.target.result);
    }
    reader.readAsDataURL(e.target.files[0]);

    $("#tenhinh").text(temp[2]);
    $("#themhinhmonan").attr('hidden', false);
});

//Xóa hình món ăn
$("#xoahinhmonan").click(function (e) {
    $("#themhinhmonan").attr('hidden', true);
});
