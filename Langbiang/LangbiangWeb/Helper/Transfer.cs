﻿using DuLichService.Models;
using LangbiangWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Web;
using LangbiangWeb.Models;

namespace LangbiangWeb.Helper
{
    public class Transfer
    {
        public static HttpClient  client =new HttpClient();
        static Transfer()
        {

            client.BaseAddress = new Uri("http://travelservice.apphb.com/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public static List<IndexLocationModel> GetIndexLocation(string searchKey, string provinceId, string categoryId, int offset, int n)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync("api/location?searchKey=" + searchKey + "&provinceId=" + provinceId + "&categoryId=" + categoryId + "&offset=" + offset + "&n=" + n).Result;
                if (response.IsSuccessStatusCode)
                {
                    var locations = response.Content.ReadAsAsync<List<IndexLocationModel>>().Result;
                    return locations;
                }
                return null;
            }
            catch { return null; }
        }

        public static LocationModel GetDetail(string id)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync("api/location?id="+id).Result;
                if (response.IsSuccessStatusCode)
                {
                    var location = response.Content.ReadAsAsync<LocationModel>().Result;
                    return location;
                }
                return null;
            }
            catch { return null; }
        }

       

        public static bool GetFavourite(string userId, string locationId)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync("api/favourite?userId=" + userId +"&locationId="+locationId).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<bool>().Result;
                    return result;
                }
                
            }
            catch { }
            return false;
        }

        public static bool GetRate(string locationId, int rate)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync("api/location?locationId=" + locationId + "&rate=" + rate).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<bool>().Result;
                    return result;
                }
                
            }
            catch { }
            return false;
        }

        public static List<IndexLocationModel> GetFavourite(string id, string searchKey, int offset, int n)
        {
            try
            {
                HttpResponseMessage response = client.GetAsync("api/favourite?id=" + id + "&searchKey=" + searchKey + "&offset=" + offset + "&n=" + n).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<IndexLocationModel>>().Result;
                    return result;
                }

            }
            catch { }
            return null;
        }

        public static  bool PostRegister(AccountModel accountModel)
        { 
             HttpResponseMessage response = client.PostAsJsonAsync("api/account", accountModel).Result;
             if (response.IsSuccessStatusCode)
             {
                 var result = response.Content.ReadAsAsync<bool>().Result;
                 return result;
             }
             return false;
        }

        public static AccountModel CheckAccount(string username)
        {
            HttpResponseMessage response = client.GetAsync("api/account?id="+username).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsAsync<AccountModel>().Result;
                return result;
            }
            return null;
        
        }
        public static bool UpdateAccount(AccountModel acc)
        {
            HttpResponseMessage response = client.PutAsJsonAsync("api/ManagerAccount", acc).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsAsync<bool>().Result;
                return result;
            } return false;
        }

        public static string getAccessToken(string username,string password)
        {
            string requestToken=getRequestToken();
            SHA1 sha=new SHA1CryptoServiceProvider();
            password = Tools.SHA1HashStringForUTF8String(requestToken + password);
            if (requestToken!=null)
            {
                HttpResponseMessage response = client.GetAsync("oauth/accesstoken?requestToken="+requestToken+"&username=" + username + "&password=" + password).Result;
                if (response.IsSuccessStatusCode)
                {
                    dynamic result = response.Content.ReadAsAsync<dynamic>().Result;
                    if ((string)result.AccessToken != null)
                    {
                        HttpResponseMessage res = client.GetAsync("oauth/SaveAccess?access=" + (string)result.AccessToken + "&expires=" + (string)result.Expires+"&refresh="+(string)result.RefreshToken).Result;
                        
                    }
                    return (string)result.AccessToken;
                }
                else return null;
            }
            else return null;
        }
        public static string getRequestToken()
        {
             HttpResponseMessage response = client.GetAsync("oauth/requesttoken").Result;
            if (response.IsSuccessStatusCode)
            {
                dynamic result=response.Content.ReadAsAsync<dynamic>().Result;
                return (string)result.RequestToken;
            }
                return null;
        }
        public static List<Province> GetProvince()
        {
            try
            {
                HttpResponseMessage response = client.GetAsync("api/province").Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<Province>>().Result;
                    return result;
                }

            }
            catch { }
            return null;
        }
    }
}