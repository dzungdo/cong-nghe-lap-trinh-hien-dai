﻿using DuLichService.Models;
using Facebook;
using LangbiangWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using LangbiangWeb.Models;

namespace LangbiangWeb.Helper
{
    public class Tools
    {
        public static dynamic Fql(FacebookClient fb, string query)
        {
            return fb.Get("fql", new { q = query });
        }
        public static string FormatHtml(List<IndexLocationModel> list)
        {
            string result = "";
            if (list!=null)
            foreach (var item in list)
            {
                result += "<li class='type-dish ui-count-3'><div class='head'><div class='img dish-img'>";
                if (item.pic != null) 
                    result +="<div class='ui-image ui-animate-opacity-250' style='background-image: url("+item.pic+"); opacity:1;'></div>}";

                result += "</div></div><div class='details'><div class='title' style='left:15px'>";
                 //<div id="@p.id" onclick="clicked(this);" class="follow-action action link-follow">
                result += "<a class='user-link internal-link' href='/Home/Detail?id=" + item.id + "'>" + item.name + "</a>";
                result +="<a class='user-location'><span class='location-icon-small'></span>"+item.province+"</a>";

                result += "</div><div class='stats'><div class='number'>" + item.rate 
                    + "</div><div class='title_feat-chef'>Rate</div></div></div><div class='overlay loggedout' style='opacity: 0;'>";
                result += "<div id='" + item.id + "'onclick='clicked(this);' class='follow-action action link-follow'><div class='icon follow'></div><a href='/Home/Detail?id=" + item.id + "'>Chi tiết</a></div></div></li>";
            }
            return result;
        }
       

        public static string SHA1HashStringForUTF8String(string s)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(s);
 
            var sha1 = SHA1.Create();
            byte[] hashBytes = sha1.ComputeHash(bytes);
 
            return HexStringFromBytes(hashBytes);
        }
        public static string HexStringFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }

        public static string ProvinceHtml(List<Province> list)
        {
            string result = "";
            if (list != null)
                foreach (var item in list)
                {
                    result += "<div class='provinceRec' onclick='clickBox(this)' onmouseover='enterBox(this)' onmouseout='leaveBox(this)' id='" + item.id + "' class=''><img src='" + item.pic + "'> <span> " + item.name + "   </span></div>";
                }
            return result;
        }

    }

}