﻿
var loginFId = "";

function InitialiseFacebook(appId) {

    window.fbAsyncInit = function () {
        FB.init({
            appId: appId, // App ID
            status: true, // check login status
            cookie: true, // enable cookies to allow the server to access the session
            xfbml: true  // parse XFBML
        });

        FB.Event.subscribe('auth.login', function (response) {
            var credentials = { uid: response.authResponse.userID, accessToken: response.authResponse.accessToken };
            SubmitLogin(credentials);
        });

        //FB.getLoginStatus(function (response) {
        //    if (response.status === 'connected')
        //    {
        //        alert("user is logged into fb");
        //        AcountInfo();
        //    }
        //    else if (response.status === 'not_authorized')
        //    {
        //        alert("user is not authorised");
        //    }
        //    else
        //    {
        //        alert("user is not conntected to facebook");
        //    }
        //});

        function SubmitLogin(credentials) {
            $.ajax({
                url: "/account/facebooklogin",
                type: "POST",
                data: credentials,
                error: function () {
                    alert("error logging in to your facebook account.");
                },
                success: function () {
                    window.location.reload();
                }
            });
        }

        $("#btn-login-facebook").on('click', function () {
            alert("Login facebook 2");
            FB.Event.subscribe('auth.login', function (response) {
                var credentials = { uid: response.authResponse.userID, accessToken: response.authResponse.accessToken };
                SubmitLogin(credentials);
            });
        });

    };

    (function (d) {
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement('script');
        js.id = id;
        js.async = true;
        js.src = "http://connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

    function AcountInfo() {
        FB.api('/me?fields=id,picture,name,link', function (p) {
            alert("Account info");
            $('#divlogout').show();
            $('#divlogin').hide();
            $('#accountNav').removeClass("togglerOpen");

            loginFId = p.id;
            $('#tinymanName').text(p.name);//Hiển thị tên
            $('#img-link').attr("href", p.link);//Hiển thị link tới trang cá nhân
            $('#tinymanPhoto').attr("src", p.picture.data.url);//Hiển thị hình ảnh cá nhân
        });
    };

}