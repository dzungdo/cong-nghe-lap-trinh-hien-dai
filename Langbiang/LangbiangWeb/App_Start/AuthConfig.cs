﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Web.WebPages.OAuth;
using LangbiangWeb.Models;
using LangbiangWeb.App_Start.MyApp.UI.Infrastructure;

namespace LangbiangWeb
{
    public static class AuthConfig
    {
        public static void RegisterAuth()
        {
            // To let users of this site log in using their accounts from other sites such as Microsoft, Facebook, and Twitter,
            // you must update this site. For more information visit http://go.microsoft.com/fwlink/?LinkID=252166

            //OAuthWebSecurity.RegisterMicrosoftClient(
            //    clientId: "",
            //    clientSecret: "");

            //OAuthWebSecurity.RegisterTwitterClient(
            //    consumerKey: "",
            //    consumerSecret: "");

            OAuthWebSecurity.RegisterFacebookClient(
                appId: "218686388303970",
                appSecret: "40aef92e3d885080a792bea3ce1a44f7");

            //OAuthWebSecurity.RegisterClient(
            //        new FacebookScopedClient(
            //            "218686388303970",
            //            "40aef92e3d885080a792bea3ce1a44f7",
            //            "Scope"),
            //        "Facebook",
            //        null);
            //OAuthWebSecurity.RegisterGoogleClient();
        }
    }
}
