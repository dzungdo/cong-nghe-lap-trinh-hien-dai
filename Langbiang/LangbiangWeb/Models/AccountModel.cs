﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class AccountModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string role_account { get; set; }
        public string full_name { get; set; }

        public AuthoModel autho { get; set; }
        public AccountModel()
        { }

        public AccountModel(string username, string password, string email, string role_account, string fullname)
        {
            this.username = username;
            this.password = password;
            this.email = email;
            this.role_account = role_account;
            this.full_name = full_name;
        }
    }
}