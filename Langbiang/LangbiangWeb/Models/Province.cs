﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LangbiangWeb.Models
{

    public class Province
    {
        public string id { get; set; }
        public string name { get; set; }
        public string pic { get; set; }
    }
}