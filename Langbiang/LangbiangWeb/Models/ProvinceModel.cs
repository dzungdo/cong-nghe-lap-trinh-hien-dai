﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class ProvinceModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string info { get; set; }
        public string pic { get; set; }

        public ProvinceModel()
        { }

       

    }
}