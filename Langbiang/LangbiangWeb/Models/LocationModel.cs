﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DuLichService.Models
{
    public class LocationModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string info { get; set; }
        public double rate { get; set; }
        public int rate_n { get; set; }
        public double geo_x { get; set; }
        public double geo_y { get; set; }
        public string id_province { get; set; }
        public string id_category { get; set; }
        public int favourite { get; set; }
        public CategoryModel category { get; set; }
        public ProvinceModel province { get; set; }
        public List<ResourceModel> resources { get; set; }
        public LocationModel() { }
     

     

    }

   
    public class ViewLocation
    {
        public ViewLocation()
        {
            Locations = new List<IndexLocationModel>();
        }
        public ViewLocation(List<IndexLocationModel> list)
        {
            Locations = list;
        }
        public List<IndexLocationModel> Locations { get; set; }

    }
}